package za.co.binarylabs.dynamicPage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynamicPageApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamicPageApplication.class, args);
	}

}
